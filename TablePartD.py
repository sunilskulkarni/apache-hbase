import happybase as hb
import sys

try:
    connection = hb.Connection()
    connection.open()
    powers_table = connection.table('powers')



    # DON'T CHANGE THE PRINT FORMAT, WHICH IS THE OUTPUT
    # OR YOU WON'T RECEIVE POINTS FROM THE GRADER
    powers_row1 = powers_table.row(b'row1')

    hero = powers_row1['personal:hero']
    power = powers_row1['personal:power']
    name = powers_row1['professional:name']
    xp = powers_row1['professional:xp']
    color = powers_row1['custom:color']

    print('hero: {}, power: {}, name: {}, xp: {}, color: {}'.format(hero, power, name, xp, color))

    powers_row19 = powers_table.row(b'row19')
    hero = powers_row19['personal:hero']
    color = powers_row19['custom:color']

    print('hero: {}, color: {}'.format(hero, color))

    hero = powers_row1['personal:hero']
    name = powers_row1['professional:name']
    color = powers_row1['custom:color']
    print('hero: {}, name: {}, color: {}'.format(hero, name, color))

except:
    print(sys.exc_info()[0])
    pass
finally:
    connection.close()
