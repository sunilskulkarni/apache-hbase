import happybase as hb
import sys
# DON'T CHANGE THE PRINT FORMAT, WHICH IS THE OUTPUT
# OR YOU WON'T RECEIVE POINTS FROM THE GRADER

try:
    connection = hb.Connection()
    connection.open()
    powers_table = connection.table('powers')

    for key, data in powers_table.scan(include_timestamp=True):
        print('Found: {}, {}'.format(key, data))
except:
    print(sys.exc_info()[0])
finally:
    connection.close()



