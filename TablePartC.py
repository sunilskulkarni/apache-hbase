import happybase as hb
import csv
import sys

#https://jarrettmeyer.com/2016/02/15/inserting-data-into-hbase-with-python



#rows = []



# for key, data in powers_table.scan():
#     print key, data



try:
    csvfile = open('input.csv', "r")
    csvreader = csv.reader(csvfile)

    connection = hb.Connection()
    # before first use:
    connection.open()
    # print(connection.tables())

    powers_table = connection.table('powers')
    powers_batch = powers_table.batch()

    for row in csvreader:
        #rows.append(row)

        # for col in row:
        #     print("%10s" % col)
        # print('\n')
        #print(row[0], row[1], row[2], row[3], row[4], row[5])
        powers_batch.put(row[0],
                         {
                            'personal:hero': row[1],
                            'personal:power': row[2],
                            'professional:name': row[3],
                            'professional:xp': row[4],
                            'custom:color': row[5]
                         }
        )

    powers_batch.send()
    # for key, data in powers_table.scan():
    #     print key, data

except:
    print(sys.exc_info()[0])
finally:
    csvfile.close()
    connection.close()



