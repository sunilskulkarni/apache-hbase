import happybase as hb
import sys
from collections import defaultdict


def hashJoin(tableA, indexA, tableB, indexB):
    count = 1
    # print(len(tableA))
    # print(len(tableB))

    h = defaultdict(list)
    # hash phase
    for s in tableA:
        #print(s)
        name = s['custom:color']
        #print(name)
        #{'professional:xp': '100', 'personal:power': 'fly', 'personal:hero': 'yes', 'custom:color': 'black', 'professional:name': 'batman'}
        h[name].append(s)

    #print(h)
    #join phase
    for rowB in tableB:
        #print(rowB['custom:color'])

        joinList = h[rowB['custom:color']]
        # print(joinList)
        # print('----------------------------------')

        if(len(joinList) > 0):
            for col in joinList:
                # print(col)
                # print(col['professional:name'])
                # print(rowB['professional:name'])
                if (col['professional:name'] != rowB['professional:name']):
                    #print(count, col[3], col[2], rowB[3], rowB[2], rowB[5])
                    print('{}, {}, {}, {}, {}'.format(rowB['professional:name'], rowB['personal:power'], col['professional:name'], col['personal:power'], rowB['custom:color']))
                    count += 1

        #print('\n')

try:
    tableA = []
    tableB = []

    connection = hb.Connection()
    connection.open()
    powers_table1 = connection.table('powers')
    powers_table2 = connection.table('powers')

    for key, dataA in powers_table1.scan():
        tableA.append(dataA)

    tableB = tableA[:]



    hashJoin(tableA, 5, tableB, 5)

except:
    print(sys.exc_info()[0])
finally:
    connection.close()


# DON'T CHANGE THE PRINT FORMAT, WHICH IS THE OUTPUT
# OR YOU WON'T RECEIVE POINTS FROM THE GRADER

# color = ???
# name = ???
# power = ???
#
# color1 = ???
# name1 = ???
# power1 = ???
#
# print('{}, {}, {}, {}, {}'.format(name, power, name1, power1, color))


