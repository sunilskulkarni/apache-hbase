**1 Overview**

Welcome to HBase Machine Practice. This MP has both java and python templates. Please use whichever you prefer.

We recommend that you take a look at the Tutorial: Introduction to HBase before beginning this assignment. Note that the tutorial refers to HDP 2.3 distro and is from 2015, and we have discontinued using the HDP environment in our course since then. Having said that, reading the tutorial and becoming familiar with the concepts is still useful.

**2 General Requirements**
Please note that our grader runs on a docker container and is NOT connected to the internet. Therefore, no additional libraries are allowed for this assignment. Also, you will NOT be allowed to create any file or folder outside the current folder (that is, you can only create files and folders in the folder that your solutions are in).

**3 Set Up HBase**
TBD

**Exercise A: Create HBase Tables**
In this exercise, you will create 2 tables as shown below. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: ```TablePartA.py``` .

Table 1: a table named “powers” in HBase with three column families as given below. For simplicity, treat all data as String (i.e., don’t use any numerical data type).

Table 2: a table named “food” in HBase with two column families as given below. For simplicity, treat all data as String (i.e., don’t use any numerical data type).

We will run python TablePartA.py .

Your output should contain:
`Created powers`
`Created food`

**Exercise B: List HBase Tables**
In this exercise, you will list all the tables created in the previous part. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: ```TablePartB.py``` . Command line and the expected output are as below

**Exercise C: Populate HBase Table with Data**
In this exercise, you will insert data in the “powers“ table created in the previous part A with following schema according to input.csv. You can assume that input.csv is in the same folder as the java files. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: TablePartC.py .
We will run ```python TablePartC.py``` .

**Exercise D: Read Data**
In this exercise, you will read some of the data in the populated “powers “ table created in part C. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: TablePartD.py .

Read the values for the following row ids in order and the given attributes:

Id: "row1", Values for (hero, power, name, xp, color)

Id: "row19", Values for (hero, color)

Id: "row1", Values for (hero, name, color)

We will run python TablePartD.py . Following is an example of output lines, please use spaces between the words. Your output should contain three lines for this exercise.

**Exercise E: Scan Data**
In this exercise, you will scan the data in the populated “powers “ table created in part C. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: TablePartE.py .

We will run python TablePartE.py . The output should look similar to below:

**Exercise F: A JOIN SQL query in HBase**
In this exercise, you will implement a SQL query in Python and run this query in the populated “powers “ table created in part C. Of course HBase does not support SQL queries. Instead, you are supposed to write the nested loop that would achieve the same results as if HBase supported SQL and could execute this command. The SQL query is as follows:

```SELECT * FROM POWERS P INNER JOIN POWERS P1 ON p.color = p1.color AND p.name <> p1.name```

To make the implementation easier, we have provided a boilerplate for this exercise in the following file: TablePartF.py. We will run python TablePartF.py . Don't use any additional libraries. The output should look similar to below and contains 56 lines in total:

